/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.labelview.slice;

import com.example.labelview.ResourceTable;
import com.lid.lib.LabelImageView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * 列表展示类
 */
public class ListViewAbilitySlice extends AbilitySlice {

    private CategoryAdapter categoryAdapter;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_list_view);
        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_listview);
        categoryAdapter = new CategoryAdapter(this, null);
        getCategoryData();
        getCategoryData();
        getCategoryData();
        listContainer.setItemProvider(categoryAdapter);
    }

    private void getCategoryData() {
        List<CategoryData> data = new ArrayList<>();
        {
            CategoryData item = new CategoryData();
            item.text = "Following the founding of New China, the policy of cultural reawakening offered Kunqu Opera the chance to emerge from a long period of neglect.";
            item.image = ResourceTable.Media_k1 + "";
            item.label = "MD";
            data.add(item);
        }
        {
            CategoryData item = new CategoryData();
            item.text = "Kunqu Opera can trace its origins back to the Ming Dynasty about six hundred years ago, and a small town south of the Yangtze, called Kunshan.";
            item.image = ResourceTable.Media_k2 + "";
            item.label = "FC";
            data.add(item);
        }

        {
            CategoryData item = new CategoryData();
            item.text = "The popularity of Kunqu Opera historically has a lot to do with the support it received from the imperial court, from the time of Emperor Kangxi, onwards.";
            item.image = ResourceTable.Media_k3 + "";
            item.label = "NFC";
            data.add(item);
        }

        {
            CategoryData item = new CategoryData();
            item.text = "Perhaps the greatest masterpiece of Kunqu Opera is “The Peony Pavilion” written by Tang Xianzu in the early years of the 17th century.";
            item.image = ResourceTable.Media_k4 + "";
            item.label = "GBA";
            data.add(item);
        }

        {
            CategoryData item = new CategoryData();
            item.text = "Kunqu Opera is remarkable, if for nothing else, because of the long time it has been around. It continues to exert a strong appeal today.";
            item.image = ResourceTable.Media_k5 + "";
            item.label = "PS";
            data.add(item);
        }

        {
            CategoryData item = new CategoryData();
            item.text = "In the late Qing Dynasty, after five hundred years of development, Kunqu faced the greatest crisis in its existence.";
            item.image = ResourceTable.Media_k6 + "";
            item.label = "XBOX";
            data.add(item);
        }


        {
            CategoryData item = new CategoryData();
            item.text = "During the Qing Dynasty that followed, it became so popular that it was said to have an influence at every level of society, from the imperial court, down.";
            item.image = ResourceTable.Media_k7 + "";
            item.label = "GB";
            data.add(item);
        }


        {
            CategoryData item = new CategoryData();
            item.text = "The Qing Dynasty’s greatest playwrights, are Hong Sheng and Kong Shangren, who wrote, respectively, “The Palace of Eternal Youth” and “The Peach Blossom Fan”.";
            item.image = ResourceTable.Media_k8 + "";
            item.label = "N64";
            data.add(item);
        }


        {
            CategoryData item = new CategoryData();
            item.text = "Every period of history has its own fashions and tastes in clothing, music, etc. During the Ming Dynasty, it was fashionable among the intelligentsia to enjoy Kunqu Opera.";
            item.image = ResourceTable.Media_k9 + "";
            item.label = "PSP";
            data.add(item);
        }

        {
            CategoryData item = new CategoryData();
            item.text = "Kunqu Opera can trace its origins back to the late Ming Dynasty and a small town south of the Yangtze, called Kunshan.";
            item.image = ResourceTable.Media_k10 + "";
            item.label = "NDS";
            data.add(item);
        }
        categoryAdapter.addAll(data);
        categoryAdapter.notifyDataChanged();
    }

    /**
     * 列表适配器
     */
    public class CategoryAdapter extends RecycleItemProvider {
        private final Context mContext;
        private ArrayList<CategoryData> listData = new ArrayList<>();

        public CategoryAdapter(ListViewAbilitySlice listViewAbilitySlice, ArrayList<CategoryData> data) {
            this.mContext = listViewAbilitySlice;
        }

        @Override
        public int getCount() {
            return listData != null ? listData.size() : 0;
        }

        @Override
        public Object getItem(int i1) {
            return listData.get(i1);
        }

        @Override
        public long getItemId(int i1) {
            return i1;
        }

        @Override
        public Component getComponent(int i1, Component component, ComponentContainer componentContainer) {
            MyViewHolder viewHolder = null;
            if (component == null) {
                component = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_list_view_item, null, false);
                viewHolder = new MyViewHolder();
                viewHolder.initView(component);
                component.setTag(viewHolder);
            } else {
                viewHolder = (MyViewHolder) component.getTag();
            }
            viewHolder.labelImageView.setPixelMap(Integer.parseInt(listData.get(i1).image));
            viewHolder.text.setText(listData.get(i1).text);
            viewHolder.labelImageView.setLabelText("LID" + i1);

            return component;
        }

        private void addAll(List<CategoryData> data) {
            listData.addAll(data);
            notifyDataChanged();
        }

        class MyViewHolder {
            private LabelImageView labelImageView;
            private Text text;

            private void initView(Component component) {
                labelImageView = (LabelImageView) component.findComponentById(ResourceTable.Id_image);
                text = (Text) component.findComponentById(ResourceTable.Id_text);
            }
        }
    }


    /**
     * 条目数据类
     */
    public class CategoryData {
        private String image;
        private String text;
        private String label;
    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
