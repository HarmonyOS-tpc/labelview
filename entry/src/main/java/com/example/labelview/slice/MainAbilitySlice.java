/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.labelview.slice;

import com.example.labelview.ResourceTable;
import com.lid.lib.LabelButtonView;
import com.lid.lib.LabelImageView;
import com.lid.lib.LabelTextView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;

/**
 * 主页面展示类
 */
public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        final LabelButtonView labelButtonView = (LabelButtonView) findComponentById(ResourceTable.Id_labelbutton);

        labelButtonView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                labelButtonView.setLabelVisual(!labelButtonView.isLabelVisual());
            }
        });

        final LabelImageView labelImageView1 = (LabelImageView) findComponentById(ResourceTable.Id_image1);
        labelImageView1.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                labelImageView1.setLabelDistance(50);
            }
        });


        final LabelImageView labelImageView2 = (LabelImageView) findComponentById(ResourceTable.Id_image2);
        labelImageView2.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                labelImageView2.setLabelText("ART");
            }
        });


        final LabelTextView labelTextView = (LabelTextView) findComponentById(ResourceTable.Id_text);
        labelTextView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                labelTextView.setLabelOrientation(3);
            }
        });
        findComponentById(ResourceTable.Id_click).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent1 = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.example.labelview")
                        .withAbilityName("com.example.labelview.ListViewAbility")
                        .build();
                intent1.setOperation(operation);
                startAbility(intent1);
            }
        });

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
