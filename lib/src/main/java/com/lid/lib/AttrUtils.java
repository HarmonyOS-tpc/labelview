/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lid.lib;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;

/**
 * 自定义属性工具类
 */
public class AttrUtils {

    /**
     * 获取自定义属性int类型返回值
     * @param attrs 自定义属性集合
     * @param name 属性名称
     * @param defaultValue 默认值
     * @return int类型值
     */
    public static int getIntFromAttr(AttrSet attrs,String name,int defaultValue){
        int value = defaultValue;
        try {
            if(attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null){
                value = attrs.getAttr(name).get().getIntegerValue();
            }
        }catch (Exception e){
            LogUtil.error("AttrUtils",e.getMessage());
        }
        return value;
    }

    /**
     * 获取自定义属性float类型返回值
     * @param attrs 自定义属性集合
     * @param name 属性名称
     * @param defaultValue 默认值
     * @return float类型值
     */
    public static float getFloatFromAttr(AttrSet attrs,String name,float defaultValue){
        float value = defaultValue;
        try {
            if(attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null){
                value = attrs.getAttr(name).get().getFloatValue();
            }
        }catch (Exception e){
            LogUtil.error("AttrUtils",e.getMessage());
        }
        return value;
    }

    /**
     * 获取自定义属性boolean类型返回值
     * @param attrs 自定义属性集合
     * @param name 属性名称
     * @param defaultValue 默认值
     * @return boolean类型值
     */
    public static boolean getBooleanFromAttr(AttrSet attrs,String name,boolean defaultValue){
        boolean value = defaultValue;
        try {
            if(attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null){
                value = attrs.getAttr(name).get().getBoolValue();
            }
        }catch (Exception e){
            LogUtil.error("AttrUtils",e.getMessage());
        }
        return value;
    }

    /**
     * 获取自定义属性long类型返回值
     * @param attrs 自定义属性集合
     * @param name 属性名称
     * @param defaultValue 默认值
     * @return long类型值
     */
    public static long getLongFromAttr(AttrSet attrs,String name,long defaultValue){
        long value = defaultValue;
        try {
            if(attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null){
                value = attrs.getAttr(name).get().getLongValue();
            }
        }catch (Exception e){
            LogUtil.error("AttrUtils",e.getMessage());
        }
        return value;
    }

    /**
     * 获取自定义属性int类型颜色返回值
     * @param attrs 自定义属性集合
     * @param name 属性名称
     * @param defaultValue 默认值
     * @return int类型颜色值
     */
    public static int getColorFromAttr(AttrSet attrs, String name, int defaultValue){
        int value = defaultValue;
        try {
            if(attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null){
                value = attrs.getAttr(name).get().getColorValue().getValue();
            }
        }catch (Exception e){
            LogUtil.error("AttrUtils",e.getMessage());
        }
        return value;
    }

    /**
     * 获取自定义属性int类型返回值
     * @param attrs 自定义属性集合
     * @param name 属性名称
     * @param defaultValue 默认值
     * @return int类型值
     */
    public static int getDimensionFromAttr(AttrSet attrs, String name, int defaultValue){
        int value = defaultValue;
        try {
            if(attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null){
                value = attrs.getAttr(name).get().getDimensionValue();
            }
        }catch (Exception e){
            LogUtil.error("AttrUtils",e.getMessage());
        }
        return value;
    }

    /**
     * 获取自定义属性String类型返回值
     * @param attrs 自定义属性集合
     * @param name 属性名称
     * @param defaultValue 默认值
     * @return String类型值
     */
    public static String getStringFromAttr(AttrSet attrs, String name, String defaultValue){
        String value = defaultValue;
        try {
            if(attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null){
                value = attrs.getAttr(name).get().getStringValue();
            }
        }catch (Exception e){
            LogUtil.error("AttrUtils",e.getMessage());
        }
        return value;
    }

    /**
     * 获取自定义属性element类型返回值
     * @param attrs 自定义属性集合
     * @param name 属性名称
     * @param defaultValue 默认值
     * @return element类型值
     */
    public static Element getElementFromAttr(AttrSet attrs, String name, Element defaultValue){
        Element value = defaultValue;
        try {
            if(attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null){
                value = attrs.getAttr(name).get().getElement();
            }
        }catch (Exception e){
            LogUtil.error("AttrUtils",e.getMessage());
        }
        return value;
    }
}
