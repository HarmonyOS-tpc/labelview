﻿# LabelView
有时，我们需要在ImageView或任何其他视图上方显示标签。LabelXXView
将会为您提供帮助，它也很容易实现！

## 主要特征
 * 支持改变标签大小
 * 支持改变标签方向
 * 支持在ImageView,button,Text上使用
 * 支持列表显示
 
 
## 演示

|功能演示 | 列表演示|
|:---:|:---:|
|<img src="https://gitee.com/openharmony-tpc/labelview/raw/master/screenshot/functionshow.gif" width="75%"/>|<img src="https://gitee.com/openharmony-tpc/labelview/raw/master/screenshot/listshow.gif" width="75%"/>|

图片过大，登录后可见
 
## entry运行要求
 通过DevEco studio,并下载openHarmonySDK
 将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本） 
 
## 集成

```
方式一：
通过library生成har包，添加har包到libs文件夹内
在entry的gradle内添加如下代码
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])

方式二：
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:labelview:1.0.2'
```

## 控件使用
#### LabelButtonView
```xml
<com.lid.lib.LabelButtonView
                ohos:id="$+id:labelbutton"
                ohos:height="48vp"
                ohos:width="200vp"
                ohos:background_element="#03a9f4"
                ohos:label_backgroundColor="#C2185B"
                ohos:label_distance="20vp"
                ohos:label_height="20vp"
                ohos:label_orientation="RIGHT_TOP"
                ohos:label_text="HD"
                ohos:label_textSize="12vp"
                ohos:text="Button"
                ohos:text_alignment="center"
                ohos:text_color="#ffffff"
        />
```

#### LabelImageView
```xml
<com.lid.lib.LabelImageView
                    ohos:id="$+id:image1"
                    ohos:height="match_content"
                    ohos:width="0vp"
                    ohos:margin="10vp"
                    ohos:image_src="$media:image1"
                    ohos:label_backgroundColor="#C2185B"
                    ohos:label_orientation="LEFT_TOP"
                    ohos:label_text="CHINA"
                    ohos:scale_mode="clip_center"
                    ohos:weight="1"
            />
```

#### LabelTextView
```xml
 <com.lid.lib.LabelTextView
                ohos:id="$+id:text"
                ohos:height="48vp"
                ohos:width="match_content"
                ohos:background_element="#212121"
                ohos:label_backgroundColor="#03A9F4"
                ohos:label_distance="15vp"
                ohos:label_orientation="LEFT_TOP"
                ohos:label_text="POP"
                ohos:label_textSize="10vp"
                ohos:layout_alignment="center"
                ohos:padding="16vp"
                ohos:text="TextView"
                ohos:text_alignment="center"
                ohos:text_color="#ffffff"
                ohos:top_margin="8vp"
        />
```

## License
```
Copyright 2014 linger1216

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```